<?php

namespace Drupal\fedora_entity\Controller;


use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\fedora_entity\FedoraObjectInterface;

class FedoraEntityViewController extends EntityViewController {

  public function label(FedoraObjectInterface $fedora_object) {
    $fedora_object->label();
  }

}
