<?php

namespace Drupal\fedora_entity\ParamConverter;


use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\ParamConverter\EntityConverter;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

class UUIDConverter extends EntityConverter implements ParamConverterInterface {

  /**
   * The entity type IDs to load by UUID.
   *
   * @var array
   */
  protected $entityTypes;

  /**
   * Overrides EntityConverter::__construct().
   *
   * @param EntityManagerInterface $entity_manager
   * @param array $entity_types
   */
  public function __construct(EntityManagerInterface $entity_manager, array $entity_types = array()) {
    $this->entityManager = $entity_manager;
    $this->entityTypes = $entity_types;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    $entity_type = substr($definition['type'], strlen('entity:'));
    if ($storage = $this->entityManager->getStorage($entity_type)) {
      // Fail gracefully if entity_load_by_uuid() returns FALSE.
      $entity = entity_load_by_uuid($entity_type, $value);
      return $entity ? $entity : NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    if (!empty($definition['type']) && strpos($definition['type'], 'entity:') === 0) {
      $entity_type = substr($definition['type'], strlen('entity:'));
      return in_array($entity_type, $this->entityTypes);
    }
    return FALSE;
  }

}
