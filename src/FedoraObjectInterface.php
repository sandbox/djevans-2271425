<?php

namespace Drupal\fedora_entity;

use Drupal\Core\Entity\EntityInterface;

interface FedoraObjectInterface extends EntityInterface {

  /**
   * Get a RDF graph for the object.
   * @return \EasyRdf_Graph $graph
   */
  public function getGraph();

  /**
   * Get the object's URI.
   *
   * @return string $uri
   */
  public function getUri();

}
