<?php

namespace Drupal\fedora_entity\Entity;


use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryException;
use Drupal\fedora_entity\FedoraObjectInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FedoraEntityStorage extends EntityStorageBase {

  /**
   * The HTTP client used to access the Fedora Web service.
   * @var \Drupal\Core\Http\Client
   */
  protected $client;

  /**
   * Instantiates a new instance of this entity controller.
   *
   * This is a factory method that returns a new instance of this object. The
   * factory should pass any needed dependencies into the constructor of this
   * object, but not the container itself. Every call to this method must return
   * a new instance of this object; that is, it may not implement a singleton.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this object should use.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   *
   * @return static
   *   A new instance of the entity controller.
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('fedora.connection')
    );
  }

  /**
   * Overrides EntityStorageBase::__construct().
   *
   * @param EntityTypeInterface $entity_type
   *   The entity type.
   * @param Object $client
   *   The HTTP client.
   */
  public function __construct(EntityTypeInterface $entity_type, $client) {
    parent::__construct($entity_type);
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  protected function has($id, EntityInterface $entity) {
    // TODO: Consider using EasyRdf client or SPARQL.
    $response = $this->client->get($id);
    return $response->getStatusCode() == 200;
  }

  /**
   * Performs storage-specific entity deletion.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   An array of entity objects to delete.
   */
  protected function doDelete($entities) {
    $ids = array_keys($entities);
    foreach ($ids as $id) {
      $this->client->delete($id);
    }
    $this->resetCache($ids);
  }

  /**
   * Performs storage-specific saving of the entity.
   *
   * @param int|string $id
   *   The original entity ID.
   * @param \Drupal\fedora_entity\FedoraObjectInterface $entity
   *   The entity to save.
   *
   * @return bool|int
   *   If the record insert or update failed, returns FALSE. If it succeeded,
   *   returns SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  protected function doSave($id, EntityInterface $entity) {
    $turtle = $this->mapToStorageRecord($entity);
    if ($entity->isNew()) {
      $response = $this->client->post($id,
        ['headers' => ['Content-Type' => 'text/turtle'], 'body' => $turtle]
      );
      if ($response->getStatusCode() == 201) {
        return SAVED_NEW;
      }
    }
    else {
      $response = $this->client->put($id,
        ['headers' => ['Content-Type' => 'text/turtle'], 'body' => $turtle]
      );
      if ($response->getStatusCode() == 204) {
        return SAVED_UPDATED;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function loadByProperties(array $values = array()) {
    $entities = array();
    if (!empty($values)) {
      // @TODO sanitise values correctly.
      $query = "SELECT ?subject WHERE {";
      foreach ($values as $property => $value) {
        $query .= '  ?subject <' . $property . '> "' . $value . '" .';
      }
      $query .= "}";

      $response = $this->client->post('/rest/fcr:sparql', [
        'headers' => [
          'Content-Type' => 'application/sparql-query',
          'Accept' => 'application/sparql-results+json',
        ],
        'body' => $query
      ]);
      $ids = array();

      if ($response->getStatusCode() == 200) {
        $json = $response->json();
        foreach ($json['results']['bindings'] as $row) {
          $ids[] = $row['subject']['value'];
        }
      }

      $entities = $this->loadMultiple($ids);
    }
    return $entities;
  }

  /**
   * Maps from an entity object to the storage record of the field data.
   *
   * @param \Drupal\fedora_entity\FedoraObjectInterface $entity
   *   The entity object.
   *
   * @return string
   *   The serialised RDF in Turtle format.
   */
  protected function mapToStorageRecord($entity) {
    $turtle = $entity->getGraph()->serialise('turtle');
    return $turtle;
  }

  /**
   * Load a specific entity revision.
   *
   * @param int $revision_id
   *   The revision id.
   *
   * @return \Drupal\Core\Entity\EntityInterface|false
   *   The specified entity revision or FALSE if not found.
   */
  public function loadRevision($revision_id) {
    // $revision_id should be in the format:
    // http://localhost:8080/path/to/resource/fcr:versions/VERSION_NAME_OR_UUID
    $response = $this->client->get($revision_id);
    if ($response->getStatusCode() != 200) {
      return FALSE;
    }
    return $this->mapFromStorageRecords($response);
  }

  /**
   * Delete a specific entity revision.
   *
   * A revision can only be deleted if it's not the currently active one.
   *
   * @param int $revision_id
   *   The revision id.
   *
   * @throws EntityStorageException
   */
  public function deleteRevision($revision_id) {
    // $revision_id should be in the format:
    // http://localhost:8080/path/to/resource/fcr:versions/VERSION_NAME_OR_UUID
    // Attempting to remove the current version returns a HTTP 400 error.
    // @TODO implement RevisionableInterface and isDefaultRevision().
    $is_default_revision = FALSE;
    if (!$is_default_revision) {
      $response = $this->client->delete($revision_id);
      if ($response->getStatusCode() == 400) {
        throw new EntityStorageException('Current revision cannot be deleted.');
      }
    }
  }

  /**
   * Gets the name of the service for the query for this entity storage.
   *
   * @return string
   *   The name of the service for the query for this entity storage.
   *
   * @throws QueryException
   */
  public function getQueryServicename() {
    return 'entity.query.sparql';
  }

  /**
   * Performs storage-specific loading of entities.
   *
   * Override this method to add custom functionality directly after loading.
   * This is always called, while self::postLoad() is only called when there are
   * actual results.
   *
   * @param array|null $ids
   *   (optional) An array of entity IDs, or NULL to load all entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Associative array of entities, keyed on the entity ID.
   */
  protected function doLoadMultiple(array $ids = NULL) {
    $records = array();
    foreach ($ids as $id) {
      $response = $this->client->get($id, ['headers' => [
        'Accept' => 'text/turtle'
      ]]);
      if ($response->getStatusCode() == 200) {
        $body = $response->getBody();
        $body->seek(0);
        $turtle = $body->getContents();
        $records[$id] = new \EasyRdf_Graph($id, $turtle, 'turtle');
      }
    }
    return $this->mapFromStorageRecords($records);
  }
}
