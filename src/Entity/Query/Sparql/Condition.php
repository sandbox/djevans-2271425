<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 26/02/15
 * Time: 11:18
 */

namespace Drupal\fedora_entity\Entity\Query\Sparql;


use Drupal\Core\Entity\Query\ConditionBase;
use Drupal\Core\Entity\Query\ConditionInterface;

class Condition extends ConditionBase implements ConditionInterface {

  /**
   * @var \Drupal\fedora_entity\Entity\Query\Sparql\Query
   */
  protected $query;

  /**
   * Queries for the existence of a field.
   *
   * @param $field
   * @param string $langcode
   * @return ConditionInterface
   * @see \Drupal\Core\Entity\Query\QueryInterface::exists()
   */
  public function exists($field, $langcode = NULL) {
    // TODO: Implement exists() method.
    // Isn't this the default for sparql?
    // Do nothing.
  }

  /**
   * Queries for the existence of a field.
   *
   * @param string $field
   * @return ConditionInterface;
   * @see \Drupal\Core\Entity\Query\QueryInterface::notexists()
   */
  public function notExists($field, $langcode = NULL) {
    // TODO: Implement notExists() method.
    // Something to do with NOT EXISTS() keyword?
  }

  /**
   * Compiles this conditional clause.
   *
   * @param $query
   *   The query object this conditional clause belongs to.
   */
  public function compile($query) {
    // TODO: Implement compile() method.
    foreach ($this->conditions as $condition) {
      $query->condition($condition['field'], $condition['value'], $confition);
    }
  }

  /**
   * Translates the string operators to SPARQL equivalents.
   *
   * @param array $condition
   *   The condition array.
   *
   * @see \Drupal\Core\Database\Query\ConditionInterface::condition()
   */
  public static function translateCondition(&$condition) {

    // There is nothing we can do for IN(). Or is there? @TODO
    if (is_array($condition['value'])) {
      return;
    }
    // Ensure that the default operator is set to simplify the cases below.
    if (empty($condition['operator'])) {
      $condition['operator'] = '=';
    }
    switch ($condition['operator']) {
      case '=':
        // Filter (?var = '')
        break;
      case '<>':
        // If a field explicitly requests that queries should not be case
        // sensitive, use the NOT LIKE operator, otherwise keep <>.
        // Filter (?var != '')
        break;
      case 'STARTS_WITH':
        // Filter REGEX(?var, '^' . pattern)
        if ($case_sensitive) {
          // add i flag
        }
        break;

      case 'CONTAINS':
        // Filter REGEX(?var, pattern)
        if ($case_sensitive) {
          // add i flag
        }
        break;

      case 'ENDS_WITH':
        // Filter REGEX(?var, pattern . '$')
        if ($case_sensitive) {
          // add i flag
        }
    }
  }
}