<?php

namespace Drupal\fedora_entity\Entity\Query\Sparql;


use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryBase;
use Drupal\Core\Entity\Query\QueryFactoryInterface;

class QueryFactory implements QueryFactoryInterface {

  /**
   * The SPARQL client to use.
   *
   * @var \EasyRdf_Sparql_Client
   */
  protected $client;

  /**
   * The namespace of this class, the parent class etc.
   *
   * @var array
   */
  protected $namespaces;

  /**
   * Constructs a QueryFactory object.
   *
   * @param \EasyRdf_Sparql_Client $client
   *   The SPARQL client used to issue queries.
   */
  public function __construct(\EasyRdf_Sparql_Client $client) {
    $this->client = $client;
    $this->namespaces = QueryBase::getNamespaces($this);
  }

  /**
   * {@inheritdoc}
   */
  public function get(EntityTypeInterface $entity_type, $conjunction) {
    $class = QueryBase::getClass($this->namespaces, 'Query');
    return new $class($entity_type, $conjunction, $this->client, $this->namespaces);
  }

  /**
   * {@inheritdoc}
   */
  public function getAggregate(EntityTypeInterface $entity_type, $conjunction) {
    $class = QueryBase::getClass($this->namespaces, 'QueryAggregate');
    return new $class($entity_type, $conjunction, $this->client, $this->namespaces);
  }

}