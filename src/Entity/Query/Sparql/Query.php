<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 26/02/15
 * Time: 11:18
 */

namespace Drupal\fedora_entity\Entity\Query\Sparql;


use Drupal\Core\Entity\Query\QueryBase;
use Drupal\Core\Entity\Query\QueryInterface;

class Query extends QueryBase implements QueryInterface {

  /**
   * @var \EasyRdf_Sparql_Client
   */
  protected $client;

  /**
   * Stores the entity manager used by the query.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Execute the query.
   *
   * @return int|array
   *   Returns an integer for count queries or an array of ids. The values of
   *   the array are always entity ids. The keys will be revision ids if the
   *   entity supports revision and entity ids if not.
   */
  public function execute() {
    // TODO: Implement execute() method.
    return $this->prepare()
      ->compile()
      ->finish()
      ->result();
  }

  public function addTriple($subject, $predicate, $object, $langcode = NULL) {
    $this->triples[] = '';
  }

  public function addTranslatedFilter($alias, $operator, $params = NULL) {
    if (!is_array($params)) {
      $params = (array) $params;
    }
    switch($operator) {
      case '<>':
        $operator = '!=';
        $value = current($params);
        $this->filters[] = "{$alias} {$operator} {$value}";
        break;
      case '<':
        // Fall through.
      case '>':
        // Fall through.
      case '<=':
        // Fall through.
      case '>=':
        $value = current($params);
        $this->filters[] = "{$alias} {$operator} {$value}";
        break;
      case 'STARTS_WITH':
        //@TODO escape for regex
        $value = current($params);
        $pattern = "^{$value}";
        $this->filters[] = "REGEX({$alias}, '{$pattern}')";
        break;
      case 'CONTAINS':
        //@TODO escape for regex
        $value = current($params);
        $pattern = $value;
        $this->filters[] = "REGEX({$alias}, '{$pattern}')";
        break;
      case 'ENDS_WITH':
        // @TODO escape for regex
        $value = current($params);
        $pattern = "{$value}$";
        // @TODO flags for regex?
        $this->filters[] = "REGEX({$alias}, '{$pattern}')";
        break;
      case 'IN':
        // @TODO
        // Fall through.
      case 'NOT IN':
        // @TODO
        break;
      case 'BETWEEN':
        if (sizeof($params) > 1) {
          list($min, $max) = $params;
          $this->filters[] = "{$alias} >= $min";
          $this->filters[] = "{$alias} <= $max";
        }
        break;
      default:
        break;
    }
  }

  function addOrder($alias, $sort = 'ASC') {
    $this->orderBy = "{$alias} {$sort}";
  }

  function addLimit($limit) {
    $this->limit = $limit;
  }

  function addOffset($offset) {
    $this->offset = $offset;
  }

}