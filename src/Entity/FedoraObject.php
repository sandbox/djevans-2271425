<?php

namespace Drupal\fedora_entity\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinition;
use Drupal\Core\Language\Language;
use Drupal\fedora_entity\FedoraObjectInterface;

/**
 * Defines the FedoraObject entity class.
 *
 * @ContentEntityType(
 *   id = "fedora_object",
 *   label_callback = "fedora_entity_label",
 *   bundle_label = @Translation("Fedora Object Type"),
 *   admin_permission = "administer fedora",
 *   fieldable = TRUE,
 *   handlers = {
 *     "storage" = "Drupal\fedora_entity\Entity\FedoraEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\fedora_entity\Entity\Form\FedoraEntityForm",
 *       "edit" = "Drupal\fedora_entity\Entity\Form\FedoraEntityForm",
 *       "delete" = "Drupal\fedora_entity\Entity\Form\FedoraEntityDeleteForm",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "http://fedora.info/definitions/v4/repository#uuid",
 *     "label" = "http://www.w3.org/2000/01/rdf-schema##label",
 *     "uuid" = "http://fedora.info/definitions/v4/repository#uuid",
 *     "revision" = "http://fedora.info/definitions/v4/repository#baseVersion",
 *   },
 *   links = {
 *     "canonical" = "/fedora_object/{fedora_object}",
 *     "delete-form" = "/fedora_object/{fedora_object}/delete",
 *     "edit-form" = "/fedora_object/{fedora_object}/edit",
 *     "version-history" = "/fedora_object/{fedora_object}/revisions",
 *   },
 * )
 */
class FedoraObject extends ContentEntityBase implements FedoraObjectInterface {

  /**
   * The internal representation of the RDF statements made about the object.
   * @var \EasyRDF_Graph
   */
  protected $graph;

  /**
   * @var string
   */
  protected $uri;

  /**
   * Overrides ContentEntityBase::__construct().
   */
  public function __construct(\EasyRdf_Graph $graph, $entity_type, $bundle = FALSE) {
    $this->entityTypeId = $entity_type;
    $this->entityKeys['bundle'] = $bundle ? $bundle : $this->entityTypeId;
    $this->languages = $this->languageManager()->getLanguages(Language::STATE_ALL);


    $this->graph = $graph;
    $this->uri = $this->graph->getUri();

    // Initialize translations. Ensure we have at least an entry for the default
    // language.
    $data = array('status' => static::TRANSLATION_EXISTING);
    $this->translations[Language::LANGCODE_DEFAULT] = $data;
    $this->setDefaultLangcode();

  }

  /**
   * Get the internal graph.
   *
   * @return \EasyRdf_Graph $graph
   *   The internal RDF graph.
   */
  public function getGraph() {
    return $this->graph;
  }

  public function getUri() {
    return $this->graph->getUri();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = array();
    // Include fields from the Fedora LDP.
    // Read-only fields: fedora:uuid, created, hasParent, ldp:contains
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel('UUID')
      ->setDescription('The unique ID of the resource.')
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel('Created')
      ->setDescription('The time of creating the resource.')
      ->setReadOnly(TRUE);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel('Parent Object')
      ->setDescription('This resource\'s parent object.')
      ->setSetting('target_type', 'fedora_entity')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ));

    $fields['children'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel('Children')
      ->setDescription('The resources contained by this resource.')
      ->setSetting('target_type', 'fedora_entity')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ));

    // Writable fields:
    // Dublin Core fields
    return $fields;
  }
}
