<?php

namespace Drupal\fedora_entity\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class FedoraConnectionSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fedora_entity_connection_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['base_url'] = array(
      '#type' => 'url',
      '#title' => t('Fedora Repository URL'),
      '#description' => t('Enter the base URL for the Fedora repository.'),
      '#required' => TRUE,
      '#default_value' => $this->config('fedora_entity.settings')
          ->get('base_url'),
    );
    $form['sparql'] = array(
      '#type' => 'fieldset',
      '#title' => t('SPARQL Settings'),
    );
    $form['sparql']['sparql_query_url'] = array(
      '#type' => 'url',
      '#title' => t('SPARQL Query URL'),
      '#description' => t('Enter the URL for the SPARQL query service'),
      '#default_value' => $this->config('fedora_entity.settings')
          ->get('sparql_select_url'),
      '#required' => TRUE,
    );
    $form['sparql']['sparql_update_url'] = array(
      '#type' => 'url',
      '#title' => t('SPARQL Update URL'),
      '#description' => t('Enter the URL for the SPARQL update service.'),
      '#default_value' => $this->config('fedora_entity.settings')
        ->get('sparql_update_url'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('fedora_entity.settings')
      ->set('base_url', $form_state->getValue('base_url'))
      ->set('sparql_select_url', $form_state->getValue('sparql_select_url'))
      ->set('sparql_update_url', $form_state->getValue('sparql_update_url'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!filter_var($form_state->getValue('base_url'), FILTER_VALIDATE_URL)) {
      $this->setFormError('base_url', t('The base URL is invalid.'));
    }
    $sparql_select_url = $form_state->getValue('sparql_select_url');
    if (!empty($sparql_select_url) && !filter_var($sparql_select_url, FILTER_VALIDATE_URL)) {
      $this->setFormError('sparql_select_url', t('The SPARQL SELECT URL is invalid.'));
    }
    $sparql_update_url = $form_state->getValue('sparql_update_url');
    if (!empty($sparql_update_url) && !filter_var($sparql_update_url, FILTER_VALIDATE_URL)) {
      $this->setFormError('sparql_update_url', t('The SPARQL UPDATE URL is invalid.'));
    }
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['fedora_entity.settings'];
  }
}
