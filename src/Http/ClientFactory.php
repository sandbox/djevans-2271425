<?php

namespace Drupal\fedora_entity\Http;


use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Http\Client;

class ClientFactory {

  public static function get(ConfigFactory $configFactory) {
    $settings = $configFactory->get('fedora_entity.settings');
    $base_url = $settings->get('base_url');
    $config = array('base_url' => $base_url);
    return new Client($config);
  }

}
