<?php

use \Drupal\fedora_entity\FedoraObjectInterface;

/**
 * Implements hook_rdf_namespaces().
 */
function fedora_entity_rdf_namespaces() {
  // The following namespaces are taken from the 'Update Properties' form field
  // in Fedora 4's REST interface.
  // The first prefix in the array has been renamed to 'dc11' in order to avoid
  // a collision with a different prefix already defined in Drupal core.
  // @see rdf_rdf_namespaces()
  $namespaces = array(
    'dc11' => 'http://purl.org/dc/elements/1.1/',
    'nt' => 'http://www.jcp.org/jcr/nt/1.0',
    'foaf' => 'http://xmlns.com/foaf/0.1/',
    'fcrepo' => 'http://fedora.info/definitions/v4/repository#',
    'ns001' => 'http://purl.org/dc/terms/',
    'test' => 'info:fedora/test/',
    'fedoraconfig' => 'http://fedora.info/definitions/v4/config#',
    'image' => 'http://www.modeshape.org/images/1.0',
    'xs' => 'http://www.w3.org/2001/XMLSchema',
    'xml' => 'http://www.w3.org/XML/1998/namespace',
    'xmlns' => 'http://www.w3.org/2000/xmlns/',
    'fedorarelsext' => 'http://fedora.info/definitions/v4/rels-ext#',
    'mix' => 'http://www.jcp.org/jcr/mix/1.0',
    'premis' => 'http://www.loc.gov/premis/rdf/v1#',
    'mode' => 'http://www.modeshape.org/1.0',
    'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    'sv' => 'http://www.jcp.org/jcr/sv/1.0',
    'authz' => 'http://fedora.info/definitions/v4/authorization#',
    'fedora' => 'http://fedora.info/definitions/v4/rest-api#',
    'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
  );
  return $namespaces;
}

/**
 * Implements hook_permission().
 */
function fedora_entity_permission() {
  $permissions = array(
    'view fedora objects' => array(
      'title' => t('View Fedora objects'),
    ),
    'administer fedora' => array(
      'title' => t('Administer Fedora'),
      'description' => t('Access the Fedora administration settings.'),
      'restrict_access' => TRUE,
    ),
  );
  return $permissions;
}

/**
 * Implements hook_theme().
 */
function fedora_entity_theme($existing, $type, $theme, $path) {
  return array(
    'fedora_object' => array(
      'render element' => 'element',
      'template' => 'fedora-object',
    ),
  );
}

function fedora_entity_label(FedoraObjectInterface $object) {
  $uri = $object->getUri();
  $label = $object->getGraph()->label($uri);
  return (empty($label)) ? 'Untitled Object' : $label;
}

function template_preprocess_fedora_object(&$variables) {
  /** @var FedoraObjectInterface $fedora_object */
  $fedora_object = $variables['element']['#fedora_object'];
  $variables['uuid'] = $fedora_object->uuid();
  $variables['label'] = $fedora_object->label();
  $variables['rdf'] = $fedora_object->getGraph()->dump();
}
